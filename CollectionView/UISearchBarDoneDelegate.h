//
//  UITextFieldDoneDelegate.h
//  SantanderBrasil
//
//  Created by Banco Santander Brasil on 16/06/16.
//  Copyright © 2016 Isban Brasil S/A. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol UISearchBarDoneDelegate <NSObject>
-(void)doneAction;
@end
