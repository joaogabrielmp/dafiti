//
//  UIStaticTableView.h
//  SantanderBrasil
//
//  Created by Banco Santander Brasil on 3/2/16.
//  Copyright © 2016 Isban Brasil S/A. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KeyboardHandlerTableView.h"

@class UIStaticTableView;
@protocol UIStaticTableViewCellProtocol <NSObject>

-(CGFloat)tableView:(UIStaticTableView *)tableView heightForCellInIndexPath:(NSIndexPath *)indexPath;

@end

@interface UIStaticTableViewSection : NSObject

@property (nonatomic,strong) NSString *headerName;
@property (nonatomic,strong) NSString *footerName;
@property (nonatomic,strong) NSMutableArray *cells;

@end

@interface UIStaticTableView : KeyboardHandlerTableView

#pragma mark - Adding and Removing
-(void)addSection:(UIStaticTableViewSection *)section;
-(void)removeSection:(UIStaticTableViewSection *)section;
-(void)addCell:(UITableViewCell *)cell onSection:(UIStaticTableViewSection *)section;
-(void)addCell:(UITableViewCell *)cell onSection:(UIStaticTableViewSection *)section animated:(BOOL)animated ;
-(void)removeCell:(UITableViewCell *)cell ofSection:(UIStaticTableViewSection *)section;
-(void)removeCell:(UITableViewCell *)cell ofSection:(UIStaticTableViewSection *)section animated:(BOOL)animated;
-(void)clean;
-(void)addSpaceWithHeight:(CGFloat)height onSection:(UIStaticTableViewSection *)section;

@end
