
#import "ProductDetailViewController.h"
#import "UIStaticTableView.h"
#import "BestBuyProductDetailCell.h"
#import <SDWebImage/UIImageView+WebCache.h>

@interface ProductDetailViewController ()

#pragma mark - ui properties
@property (weak, nonatomic) IBOutlet UIStaticTableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (strong, nonatomic) BestBuyProductDetailCell *bestBuyProductDetailCell;

#pragma mark - properties
@property(nonatomic,strong)id<BestBuyProductProtocol> product;

@end

@implementation ProductDetailViewController

#pragma mark - constructors
-(instancetype)initWithProduct:(id<BestBuyProductProtocol>)product {
    if (self = [super init]) {
        [self setProduct:product];
    }
    return self;
}

#pragma mark - life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Detalhe";
    if ([self.product image] != [NSNull null]) {
        [self.imageView sd_setImageWithURL:[NSURL URLWithString:[self.product image]]
                          placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    } else {
        [self.imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://placehold.it/%.f?text=Nenhuma+Imagem+Encontrada",self.imageView.frame.size.width]]
                          placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    }

    [self.tableView clean];
    UIStaticTableViewSection *section = [[UIStaticTableViewSection alloc]init];
    BestBuyProductDetailCell *bestBuyProductDetailCell = [[BestBuyProductDetailCell alloc]initWithProduct:self.product];
    [self setBestBuyProductDetailCell:bestBuyProductDetailCell];
    [self.tableView addCell:self.bestBuyProductDetailCell onSection:section];
    
    [self.tableView addSection:section];
    [self.tableView reloadData];
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
}

#pragma mark - dealloc
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)dealloc {
    
}

@end
