

#import <Foundation/Foundation.h>
#import "BestBuyProductProtocol.h"
@protocol CollectionViewDataSourceDelegate <NSObject>
-(void)didSelectBestBuyProduct:(id<BestBuyProductProtocol>)bestBuyProduct;
-(void)didScroll:(UIScrollView *)scrollView;
@end
