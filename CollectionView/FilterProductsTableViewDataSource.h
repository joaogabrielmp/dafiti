

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "FilterProductsTableViewDataSourceDelegate.h"
@interface FilterProductsTableViewDataSource : NSObject<UITableViewDataSource,UITableViewDelegate>

#pragma mark - properties
@property(nonatomic,weak)id<FilterProductsTableViewDataSourceDelegate> filterProductsTableViewDataSourceDelegate;

#pragma mark - construtors
-(instancetype)initWithTableView:(UITableView *)tableView;

#pragma mark - methods
-(NSArray<NSString *> *)getFiltersArray;
-(void)filters:(NSArray <NSString *>*)filters;


@end
