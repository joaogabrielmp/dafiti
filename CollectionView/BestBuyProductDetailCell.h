
#import <UIKit/UIKit.h>
#import "BestBuyProductProtocol.h"
#import "UIStaticTableView.h"
@interface BestBuyProductDetailCell : UITableViewCell<UIStaticTableViewCellProtocol>
-(instancetype)initWithProduct:(id<BestBuyProductProtocol>)product;
@end
