//
//  UITextField+Done.m
//  SantanderBrasil
//
//  Created by Banco Santander Brasil on 3/17/16.
//  Copyright © 2016 Isban Brasil S/A. All rights reserved.
//

#import "UISearchBar+Done.h"
#import <objc/runtime.h>
static void const *key;
@implementation UISearchBar (Done)
//@dynamic doneDelegate;
-(void)addDoneButtonAndAutomaticallyResign {
    UIBarButtonItem *doneButton = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone target:self action:@selector(doneAction)];
    UIToolbar *toolbar = [[UIToolbar alloc] init];
    [toolbar sizeToFit];
    [toolbar setItems:@[doneButton]];
    [self setInputAccessoryView:toolbar];
}
-(id<UISearchBarDoneDelegate>)doneDelegate
{
    return objc_getAssociatedObject(self, key);
}

- (void)setDoneDelegate:(id<UISearchBarDoneDelegate>)doneDelegate {
    objc_setAssociatedObject(self, key, doneDelegate, OBJC_ASSOCIATION_RETAIN);
}
-(void)doneAction {
    [self resignFirstResponder];
    [self.doneDelegate doneAction];
}

@end
