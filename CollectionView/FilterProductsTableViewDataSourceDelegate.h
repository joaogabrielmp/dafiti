

#import <Foundation/Foundation.h>

@protocol FilterProductsTableViewDataSourceDelegate <NSObject>
-(void)filterSelected:(NSString *)filterSelected;
@end
