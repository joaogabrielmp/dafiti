
#import <UIKit/UIKit.h>

@interface BestBuyProductCollectionCell : UICollectionViewCell
-(void)setImageURL:(NSString *)imageURL;
-(void)setBrandProduct:(NSString *)brand;
-(void)setNameProduct:(NSString *)name;
-(void)setPriceProduct:(NSString *)price;
@end
