//
//  UIColor+Color.m
//  Rankim
//
//  Created by Rafael Gonzalves on 3/30/16.
//  Copyright © 2016 Macbook Pro. All rights reserved.
//

#import "UIColor+Color.h"

@implementation UIColor (Color)
+(UIColor *)colorWithRGBRed:(float)red green:(float)green blue:(float)blue andAlpha:(float)alpha {
    return [UIColor colorWithRed:red/255.0f green:green/255.0f blue:blue/255.0f alpha:alpha];
}
+(UIColor *)Color {
    return [UIColor colorWithRGBRed:205 green:220 blue:57 andAlpha:1.000];
}
+(UIColor *)ColorDark {
    return [UIColor colorWithRGBRed:192 green:202 blue:51 andAlpha:1.000];
}
+(UIColor *)ColorLight {
    return [UIColor colorWithRGBRed:212 green:225 blue:87 andAlpha:1.000];
}

@end
