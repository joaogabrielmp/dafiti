

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "BestBuyProductProtocol.h"
#import "CollectionViewDataSourceDelegate.h"
@interface CollectionViewDataSource : NSObject

#pragma mark - properties
@property(nonatomic,weak)id<CollectionViewDataSourceDelegate> delegate;

#pragma mark - constructors
-(instancetype)initWithCollectionView:(UICollectionView *)collectionView;
-(void)setProducts:(NSArray<BestBuyProductProtocol>*)products;
-(NSArray<BestBuyProductProtocol>*)getProducts;
@end
