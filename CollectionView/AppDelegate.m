
#import "AppDelegate.h"
#import "ListProductsViewController.h"
#import "UIColor+Color.h"
@interface AppDelegate ()
@property(nonatomic,strong)ListProductsViewController *collectionViewController;
@property(nonatomic,strong)UINavigationController *navController;
@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self setNavigationControllerSetting];
    
    return YES;
    
}
-(void)setNavigationControllerSetting {
    self.window = [[UIWindow alloc]initWithFrame:[[UIScreen mainScreen]bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    self.collectionViewController = [[ListProductsViewController alloc]init];
    self.navController = [[UINavigationController alloc] initWithRootViewController:self.collectionViewController];
    
    [self.window setRootViewController:self.navController];
    [self applicationApplyAppearanceChanges];
    
}
-(void)applicationApplyAppearanceChanges {
    [[UINavigationBar appearance] setTintColor:[UIColor colorWithRed:0.3147 green:0.3147 blue:0.3147 alpha:1.0]];
    [[UINavigationBar appearance] setBarTintColor:[UIColor colorWithRed:0.9609 green:0.9756 blue:0.9753 alpha:1.0]];
    [[UINavigationBar appearance]  setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:0.3147 green:0.3147 blue:0.3147 alpha:1.0]}];
    [[UITextField appearance] setTintColor:[UIColor ColorLight]];
    [[UIToolbar appearance] setTintColor:[UIColor ColorDark]];
    [[UINavigationBar appearance]setTranslucent:NO];
    
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
