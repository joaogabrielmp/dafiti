

#import "PopoverViewController.h"
#import "FilterProductsViewControllerProtocol.h"
@interface FilterProductsViewController : PopoverViewController<FilterProductsViewControllerProtocol>

#pragma mark - properties
@property(nonatomic,weak)id<FilterProductsViewControllerProtocol> protocol;

#pragma mark - ui properties
@property (weak, nonatomic) IBOutlet UITableView *tableView;

#pragma mark - methods
-(void)setDataSource:(id)dataSource;
@end
