
#import <UIKit/UIKit.h>
#import "BestBuyProductProtocol.h"
@interface ProductDetailViewController : UIViewController
-(instancetype)initWithProduct:(id<BestBuyProductProtocol>)product;
@end
