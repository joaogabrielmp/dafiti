

#import "CollectionViewDataSource.h"
#import "BestBuyProductCollectionCell.h"
#import "BestBuyProductProtocol.h"

@interface CollectionViewDataSource ()<UICollectionViewDataSource,UICollectionViewDelegate,UISearchBarDelegate>
#pragma mark - ui properties

@property(nonatomic,strong)UICollectionView *collectionView;
@property(nonatomic,strong)NSMutableArray<BestBuyProductProtocol> *bestBuyProducts;
@end

@implementation CollectionViewDataSource

#pragma mark - contructors
-(instancetype)initWithCollectionView:(UICollectionView *)collectionView {
    if (self = [super init]) {
        self.collectionView = collectionView;
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        [self.collectionView registerNib:[UINib nibWithNibName:@"BestBuyProductCollectionCell" bundle:nil] forCellWithReuseIdentifier:@"cell"];
        UICollectionViewFlowLayout *flow = [[UICollectionViewFlowLayout alloc] init];
        flow.scrollDirection = UICollectionViewScrollDirectionVertical;
        flow.minimumInteritemSpacing = 4;
        flow.minimumLineSpacing = 8;
        flow.sectionHeadersPinToVisibleBounds = NO;
        
        self.collectionView.collectionViewLayout = flow;
        [self.collectionView registerClass:[UICollectionReusableView class] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"HeaderView"];
    }

    return self;
}
-(void)setProducts:(NSArray<BestBuyProductProtocol> *)products {
    [self setBestBuyProducts:[products mutableCopy]];
    [self.collectionView reloadData];
}

-(NSArray<BestBuyProductProtocol> *)getProducts {
    return self.bestBuyProducts;
}
#pragma mark - collection view data source
-(NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.bestBuyProducts count];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    BestBuyProductCollectionCell * cell = (BestBuyProductCollectionCell*)[collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    id<BestBuyProductProtocol> bestBuyProduct= self.bestBuyProducts[indexPath.row];
    [cell setImageURL:[bestBuyProduct image]];
    [cell setBrandProduct:[bestBuyProduct brand]];
    [cell setNameProduct:[bestBuyProduct name]];
    [cell setPriceProduct:[bestBuyProduct salePrice]];
    return cell;
}

- (BOOL)allowsHeaderViewsToFloat{
    return NO;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(8, 8, 8, 8);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    int cellsAcross = 2;
    int spaceBetweenCells = 24;
    float width = (collectionView.bounds.size.width - (cellsAcross - 1) * spaceBetweenCells) / cellsAcross;
    return CGSizeMake(width, 280);
    
}

#pragma mark - collection view delegate
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    id<BestBuyProductProtocol> bestBuyProduct= self.bestBuyProducts[indexPath.row];
    [self.delegate didSelectBestBuyProduct:bestBuyProduct];
}

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.delegate didScroll:scrollView];
}

@end
