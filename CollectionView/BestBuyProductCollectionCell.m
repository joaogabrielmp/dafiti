
#import "BestBuyProductCollectionCell.h"
#import <SDWebImage/UIImageView+WebCache.h>
@interface BestBuyProductCollectionCell()
#pragma mark - ui properties
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *brandLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@end
@implementation BestBuyProductCollectionCell

#pragma mark - life cycle
- (void)awakeFromNib {
    [super awakeFromNib];

}

#pragma mark - setters
-(void)setImageURL:(NSString *)imageURL {
    if (imageURL != [NSNull null]) {
    [self.imageView sd_setImageWithURL:[NSURL URLWithString:imageURL]
                                      placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    } else {
        [self.imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://placehold.it/%.f?text=Nenhuma+Imagem+Encontrada",self.imageView.frame.size.width]]
                          placeholderImage:[UIImage imageNamed:@"placeholder.png"]];
    }
}

-(void)setNameProduct:(NSString *)name{

    [self.nameLabel setText:name];

}

-(void)setBrandProduct:(NSString *)brand{
    [self.brandLabel setText:brand];
}

-(void)setPriceProduct:(NSString *)price {
    [self.priceLabel setText:[NSString stringWithFormat:@"U$%.2f",[price floatValue]]];
}
@end
