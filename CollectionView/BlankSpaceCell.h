//
//  BlankSpaceCell.h
//  SantanderBrasil
//
//  Created by Banco Santander Brasil on 8/3/16.
//  Copyright © 2016 Isban Brasil S/A. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIStaticTableView.h"

@interface BlankSpaceCell : UITableViewCell

<UIStaticTableViewCellProtocol>

#pragma mark - constructor
-(instancetype)initWithHeight:(CGFloat)height;
-(instancetype)init __attribute__ ((unavailable("initWithHeight ...")));

@end
