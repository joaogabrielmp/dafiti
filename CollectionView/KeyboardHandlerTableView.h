//
//  KeyboardHandlerTableView.h
//  SantanderBrasil
//
//  Created by Banco Santander Brasil on 8/5/16.
//  Copyright © 2016 Isban Brasil S/A. All rights reserved.
//

#import <UIKit/UITableView.h>
#import <UIKit/UIKit.h>
@interface KeyboardHandlerTableView : UITableView

@end
