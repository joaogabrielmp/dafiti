//
//  WeekDayViewControllerProtocol.h
//  Agenda
//
//  Created by Banco Santander Brasil on 18/07/16.
//  Copyright © 2016 Macbook Pro. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@protocol FilterProductsViewControllerProtocol <NSObject>
-(void)setTitleText:(NSString *)titleText;
-(void)setTitleTextColor:(UIColor *)titleTextColor;

@end
