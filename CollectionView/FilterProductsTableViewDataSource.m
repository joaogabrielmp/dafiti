

#import "FilterProductsTableViewDataSource.h"

@interface FilterProductsTableViewDataSource ()

#pragma mark properties
@property(nonatomic,strong)NSMutableArray<NSString *> *arrayFilters;
@property(nonatomic,weak)UITableView *tableView;
@end

@implementation FilterProductsTableViewDataSource
#pragma mark - construtors
-(instancetype)initWithTableView:(UITableView *)tableView {
    self = [super init];
    if (self) {

    }
    return self;
}

#pragma mark - methods
-(void)filters:(NSArray<NSString *> *)filters {
    [self setArrayFilters:[filters mutableCopy]];
}
-(NSArray<NSString *> *)getFiltersArray {
    return self.arrayFilters;
}

#pragma mark - table view delegate

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return self.arrayFilters.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"filterTableViewCell";
    UITableViewCell *filterCell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    NSString *filter = self.arrayFilters[indexPath.row];
    
    if (filterCell == nil) {
        filterCell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    [filterCell.textLabel setText:filter];
    [filterCell.textLabel setFont:[UIFont italicSystemFontOfSize:13.0f]];
    [filterCell setSelectionStyle:UITableViewCellSelectionStyleNone];
    [self.tableView endUpdates];
    return filterCell;
    
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *filter = self.arrayFilters[indexPath.row];
    [self.filterProductsTableViewDataSourceDelegate filterSelected:filter];
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}
@end