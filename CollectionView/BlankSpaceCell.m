//
//  BlankSpaceCell.m
//  SantanderBrasil
//
//  Created by Banco Santander Brasil on 8/3/16.
//  Copyright © 2016 Isban Brasil S/A. All rights reserved.
//

#import "BlankSpaceCell.h"

@interface BlankSpaceCell ()

@property (nonatomic,assign) CGFloat height;

@end

@implementation BlankSpaceCell

#pragma mark - constructor
-(instancetype)initWithHeight:(CGFloat)height {
    if (self = [super init]) {
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        self.backgroundColor = [UIColor whiteColor];
        _height = height;
    } return self;
}

#pragma mark - height
-(CGFloat)tableView:(UIStaticTableView *)tableView heightForCellInIndexPath:(NSIndexPath *)indexPath {
    return self.height;
}

@end
