

#import "BestBuyProductsRepository.h"
#import "ProductParser.h"

@implementation BestBuyProductsRepository

+(void)getBestBuyDepartamentProducts:(void (^)(NSArray<BestBuyProductProtocol>*))_success failure:(void (^)(NSString *))_failure {
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

    [manager GET:@"https://api.bestbuy.com/v1/products(categoryPath.name=All%20Flat-Panel%20TVs)?show=all&format=json&apiKey=hz7ubm2z7yepkj9evg88x9fk" parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        _success([ProductParser bestBuyProductsParsed:responseObject[@"products"]]);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        _failure([error description]);
    }];
}





@end
