
#import <Foundation/Foundation.h>
#import "BestBuyProductProtocol.h"
@interface ProductParser : NSObject
+(NSArray<BestBuyProductProtocol>*)bestBuyProductsParsed:(id)response;
@end
