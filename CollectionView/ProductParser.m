
#import "ProductParser.h"
#import "BestBuyProduct.h"
@implementation ProductParser
+(NSArray<BestBuyProductProtocol>*)bestBuyProductsParsed:(id)response {
    NSMutableArray *products = [NSMutableArray new];
    for (NSDictionary *product in response) {
        BestBuyProduct *bestBuyProduct = [[BestBuyProduct alloc]init];
        product[@"name"] ? [bestBuyProduct setName:product[@"name"]] : nil;
        product[@"salePrice"] ? [bestBuyProduct setSalePrice:product[@"salePrice"]] : nil;
        product[@"thumbnailImage"] ? [bestBuyProduct setThumbnailImage:product[@"thumbnailImage"]] : nil;
        product[@"image"] ? [bestBuyProduct setImage:product[@"image"]] : nil;
        product[@"url"] ? [bestBuyProduct setUrlDetail:product[@"url"]] : nil;
        product[@"longDescription"] ? [bestBuyProduct setProductDescription:product[@"longDescription"]] : nil;
        for (NSDictionary *detail in product[@"details"]) {
            if ([detail[@"name"] isEqualToString:@"Brand"]) {
                detail[@"value"] ? [bestBuyProduct setBrand:detail[@"value"]] : nil;
            }
        }
        
        [products addObject:bestBuyProduct];
    }
    
    return [products copy];
}

@end
