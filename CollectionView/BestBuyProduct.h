

#import <Foundation/Foundation.h>
#import "BestBuyProductProtocol.h"
@interface BestBuyProduct : NSObject<BestBuyProductProtocol>
@property(nonatomic,strong)NSString *brand;
@property(nonatomic,strong)NSString *name;
@property(nonatomic,strong)NSString *salePrice;
@property(nonatomic,strong)NSString *thumbnailImage;
@property(nonatomic,strong)NSString *image;
@property(nonatomic,strong)NSString *urlDetail;
@property(nonatomic,strong)NSString *productDescription;
@end
