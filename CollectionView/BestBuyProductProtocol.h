

#import <Foundation/Foundation.h>

@protocol BestBuyProductProtocol <NSObject>
-(NSString *)brand;
-(NSString *)productDescription;
-(NSString *)name;
-(NSString *)salePrice;
-(NSString *)thumbnailImage;
-(NSString *)image;
-(NSString *)urlDetail;
@end
