//
//  UITextField+Done.h
//  SantanderBrasil
//
//  Created by Banco Santander Brasil on 3/17/16.
//  Copyright © 2016 Isban Brasil S/A. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UISearchBarDoneDelegate.h"
@interface UISearchBar (Done)

-(void)addDoneButtonAndAutomaticallyResign;
@property(nonatomic,weak)id<UISearchBarDoneDelegate> doneDelegate;
@end
