

#import <Foundation/Foundation.h>
#import "BestBuyProductProtocol.h"
@protocol BestBuyProductsRepositoryProtocol <NSObject>
+(void)getBestBuyDepartamentProducts:(void (^) (NSArray<BestBuyProductProtocol> *products)) _success failure:(void (^) (NSString* error))_failure;

@end
