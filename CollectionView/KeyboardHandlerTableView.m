//
//  KeyboardHandlerTableView.m
//  SantanderBrasil
//
//  Created by Banco Santander Brasil on 8/5/16.
//  Copyright © 2016 Isban Brasil S/A. All rights reserved.
//

#import "KeyboardHandlerTableView.h"

@implementation KeyboardHandlerTableView

#pragma mark - override
-(void)didMoveToSuperview {
    [super didMoveToSuperview];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(didKeyboardOpen:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willKeyboardClose:) name:UIKeyboardWillHideNotification object:nil];
}
-(void)removeFromSuperview {
    [super removeFromSuperview];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - notifications
-(void)didKeyboardOpen:(NSNotification *)notification {
    UIWindow *window = [[[UIApplication sharedApplication] delegate] window];
    CGRect kbFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect kbIntersectFrame = [window convertRect:CGRectIntersection(window.frame, kbFrame) toView:self];
    kbIntersectFrame = CGRectIntersection(self.bounds, kbIntersectFrame);
    
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbIntersectFrame.size.height, 0.0);
    self.contentInset = contentInsets;
    self.scrollIndicatorInsets = contentInsets;
}
-(void)willKeyboardClose:(NSNotification *)notification {
    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0);
    self.contentInset = contentInsets;
    self.scrollIndicatorInsets = contentInsets;
}


@end
