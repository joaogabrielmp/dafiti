

#import "FilterProductsViewController.h"
#import "UIColor+Color.h"
@interface FilterProductsViewController ()<FilterProductsViewControllerProtocol>
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@end

@implementation FilterProductsViewController

#pragma mark - constructors
-(instancetype)init{
    if (self = [super initWithSize:CGSizeMake([UIScreen mainScreen].bounds.size.width * .8, [UIScreen mainScreen].bounds.size.height * .6)]) {
        [self setProtocol:self];
        [self setFont];
    }
    return self;
}
-(void)setDataSource:(id)dataSource {
    [self.tableView setDelegate:dataSource];
    [self.tableView setDataSource:dataSource];
    self.tableView.separatorColor = [UIColor Color];

    self.tableView.tableFooterView = [[UIView alloc]
                                      initWithFrame:CGRectZero];

}
#pragma mark - life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

}

#pragma mark - font
-(void)setFont {
    [self.titleLabel setFont:[UIFont italicSystemFontOfSize:15.0f]];
}
#pragma mark - protocol
-(void)setTitleText:(NSString *)titleText {
    [self.titleLabel setText:titleText];
}

-(void)setTitleTextColor:(UIColor *)titleTextColor {
    [self.titleLabel setTextColor:titleTextColor];
    
}


#pragma mark - dealloc
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)dealloc {
    
}

@end
