
//

#import "ListProductsViewController.h"
#import "BestBuyProductsRepository.h"
#import "CollectionViewDataSource.h"
#import "UIColor+Color.h"
#import "CollectionViewDataSourceDelegate.h"
#import "UISearchBarDoneDelegate.h"
#import "UISearchBar+Done.h"
#import "FilterProductsViewController.h"
#import "FilterProductsTableViewDataSource.h"
#import "ProductDetailViewController.h"
@interface ListProductsViewController ()<CollectionViewDataSourceDelegate,UISearchBarDelegate, UISearchBarDoneDelegate,FilterProductsTableViewDataSourceDelegate>

#pragma mark - ui properties
@property (weak, nonatomic) IBOutlet UILabel *alertLabel;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UIButton *filterButton;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *filterTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *searchBarHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *listCollectionViewBottomConstraint;

#pragma mark - properties
@property (nonatomic, strong)CollectionViewDataSource *collectionViewDataSource;
@property (nonatomic, strong)FilterProductsViewController *filterProductsViewController;
@property (nonatomic, strong)FilterProductsTableViewDataSource *filterProductsTableViewDataSource;
@property (nonatomic, strong)NSMutableArray<BestBuyProductProtocol> *bestBuyProducts;
@property(nonatomic)int lastContentOffset;
@end

@implementation ListProductsViewController

#pragma mark - life cycle
- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"Lista de Produtos";
    self.automaticallyAdjustsScrollViewInsets = NO;
    [self.searchBar setDelegate:self];
    [self.searchBar addDoneButtonAndAutomaticallyResign];
    [self.searchBar setDoneDelegate:self];
    [self setCollectionViewDataSource];
    [self getProducts];
    [self setBorders];
    [self.searchBarHeightConstraint setConstant:0];
    [self hideButtonsWithTime:0.0];
    [self.view layoutIfNeeded];


}

-(void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
        [self.collectionView setAlwaysBounceVertical:YES];
        [self showButtons];
}

-(void)setCollectionViewDataSource {
    self.collectionViewDataSource = [[CollectionViewDataSource alloc]initWithCollectionView:self.collectionView];
    [self.collectionViewDataSource setDelegate:self];


}

-(void)setBorders {
    [self.collectionView.layer setCornerRadius:5.0f];
    UIBezierPath *searchShapePath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(self.searchButton.bounds.origin.x,self.searchButton.bounds.origin.y,self.searchButton.bounds.size.width,self.searchButton.bounds.size.height)
                                                    byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight
                                                          cornerRadii:CGSizeMake(20.0, 20.0)];
    
    CAShapeLayer *searchShapeLayer = [CAShapeLayer layer];
    searchShapeLayer.frame = self.searchButton.bounds;
    searchShapeLayer.path = searchShapePath.CGPath;
    searchShapeLayer.fillColor = [UIColor Color].CGColor;
    [self.searchButton.layer insertSublayer:searchShapeLayer atIndex:0];
    
    UIBezierPath *filterShapePath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(self.filterButton.bounds.origin.x,self.filterButton.bounds.origin.y,self.filterButton.bounds.size.width,self.filterButton.bounds.size.height)
                                                    byRoundingCorners:UIRectCornerTopLeft | UIRectCornerTopRight | UIRectCornerBottomLeft | UIRectCornerBottomRight
                                                          cornerRadii:CGSizeMake(20.0, 20.0)];
    
    CAShapeLayer *filterShapeLayer = [CAShapeLayer layer];
    filterShapeLayer.frame = self.filterButton.bounds;
    filterShapeLayer.path = filterShapePath.CGPath;
    filterShapeLayer.fillColor = [UIColor Color].CGColor;
    [self.filterButton.layer insertSublayer:filterShapeLayer atIndex:0];

}

#pragma mark - services
-(void)getProducts {
    [BestBuyProductsRepository getBestBuyDepartamentProducts:^(NSArray<BestBuyProductProtocol> *products) {
        self.bestBuyProducts = [NSMutableArray<BestBuyProductProtocol> new];
        self.bestBuyProducts = [products mutableCopy];
        [self.collectionViewDataSource setProducts:[self.bestBuyProducts copy]];
        if(self.bestBuyProducts.count>0) {
            [self showButtons];
        } else {
            [self.alertLabel setText:@"Nenhum Produto encontrado.."];
            [self.collectionView setHidden:YES];
        }
        
    } failure:^(NSString *error) {
        [self.collectionView setHidden:YES];
        [self.alertLabel setText:@"Nenhum Produto encontrado.."];
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Atenção"
                                              message:error
                                              preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       NSLog(@"OK action");
                                   }];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }];
}

- (IBAction)searchAction:(id)sender {
    [self showSearchBar];
    [self.searchBar becomeFirstResponder];
}

- (IBAction)filterAction:(id)sender {
    FilterProductsViewController *filterProductsViewController = [[FilterProductsViewController alloc] init];
    filterProductsViewController.animationType = PopoverViewControllerAnimationTypeCoverVertical;
    filterProductsViewController.shadow = YES;
    filterProductsViewController.shouldDismissOnBackgroundTap = YES;
    filterProductsViewController.cornerRadius = 12.0f;
    [self presentViewController:filterProductsViewController animated:YES completion:nil];
    FilterProductsTableViewDataSource *filterProductsTableViewDataSource = [[FilterProductsTableViewDataSource alloc]initWithTableView:filterProductsViewController.tableView];
    [filterProductsTableViewDataSource filters:@[@"Valor Crescente",@"Valor Decrescente"]];
    [filterProductsViewController setDataSource:filterProductsTableViewDataSource];
    [filterProductsTableViewDataSource setFilterProductsTableViewDataSourceDelegate:self];
    [self setFilterProductsViewController:filterProductsViewController];
    [self setFilterProductsTableViewDataSource:filterProductsTableViewDataSource];
    [self.filterProductsViewController.protocol setTitleText:@"Filtros"];
    [self.filterProductsViewController.protocol setTitleTextColor:[UIColor ColorDark]];
    
    [self.filterProductsViewController.tableView reloadData];
    

}

#pragma mark  - collectionViewDataSource delegate
-(void)didSelectBestBuyProduct:(id<BestBuyProductProtocol>)bestBuyProduct {
    ProductDetailViewController *productDetailViewController = [[ProductDetailViewController alloc]initWithProduct:bestBuyProduct];
    [self.navigationController pushViewController:productDetailViewController animated:YES];
}

-(void)didScroll:(UIScrollView *)scrollView {
    
    if (scrollView.contentOffset.y < -55) {
        [self showSearchBar];
    }
    
    if (self.lastContentOffset > scrollView.contentOffset.y )
    {
        [self showButtons];
    }
    else if (self.lastContentOffset < scrollView.contentOffset.y && scrollView.contentOffset.y >0)
    {
        [self hideButtonsWithTime:0.4];
        [self hideSearchBar];
    }
    
    self.lastContentOffset = scrollView.contentOffset.y;
}

#pragma mark - search bar delegate
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSArray *bestBuyProductsNameFilteredArray = self.bestBuyProducts;
    
    if (![searchText isEqualToString:@""]) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"name CONTAINS[cd] %@", searchText];
        bestBuyProductsNameFilteredArray = [bestBuyProductsNameFilteredArray filteredArrayUsingPredicate:predicate];
        if (bestBuyProductsNameFilteredArray.count == 0) {
            [self.alertLabel setText:@"Nenhum Produto encontrado.."];
            [self.collectionView setHidden:YES];
        } else {
            [self.collectionView setHidden:NO];
        }

        [self.collectionViewDataSource setProducts:[bestBuyProductsNameFilteredArray mutableCopy]];
    } else {
        [self.collectionView setHidden:NO];
        [self.collectionViewDataSource setProducts:self.bestBuyProducts];
    }

}
-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
  
    if (UIDeviceOrientationIsLandscape([UIDevice currentDevice].orientation))
    {
        [self.listCollectionViewBottomConstraint setConstant:216];
    } else {
        [self.listCollectionViewBottomConstraint setConstant:268];
    }
    [self.view layoutIfNeeded];
}
#pragma mark - search bar done delegate
-(void)doneAction {
    [self.listCollectionViewBottomConstraint setConstant:8];
    [self.view layoutIfNeeded];
    [self.collectionView reloadData];
}

#pragma mark - filter delegate
-(void)filterSelected:(NSString *)filterSelected {
    if ([filterSelected isEqualToString:@"Valor Crescente"]) {
        [self sortPriceInOrderASC:YES];
    } else {
        [self sortPriceInOrderASC:NO];
    }
}

-(void)sortPriceInOrderASC:(BOOL) orderASC{
    NSArray *bestBuyProductsNPriceFilteredArray = [self.collectionViewDataSource getProducts];
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"salePrice"
                                                                   ascending:orderASC];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
    NSArray *sortedArray = [bestBuyProductsNPriceFilteredArray sortedArrayUsingDescriptors:sortDescriptors];
    [self.collectionViewDataSource setProducts:[sortedArray mutableCopy]];
    [self.filterProductsViewController dismissViewControllerAnimated:YES completion:nil];

}
#pragma mark - set layout
-(void)showButtons {
    [self.filterTopConstraint setConstant:55];
    [self.searchTopConstraint setConstant:55];
    [UIView animateWithDuration:0.4 animations:^{
        [self.view layoutIfNeeded];
    }];
}

-(void)hideButtonsWithTime:(float)timer {
    [self.filterTopConstraint setConstant:200];
    [self.searchTopConstraint setConstant:200];
    [UIView animateWithDuration:timer animations:^{
        [self.view layoutIfNeeded];
    }];
}

-(void)showSearchBar {
    [self.searchBarHeightConstraint setConstant:44];
    [UIView animateWithDuration:0.4 animations:^{
        [self.view layoutIfNeeded];
    }];
}

-(void)hideSearchBar {
    [self.searchBarHeightConstraint setConstant:0];
    [UIView animateWithDuration:0.4 animations:^{
        [self.view layoutIfNeeded];
    }];
}
#pragma mark - dealloc
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
