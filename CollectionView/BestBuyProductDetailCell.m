
#import "BestBuyProductDetailCell.h"

@interface BestBuyProductDetailCell ()
#pragma mark - ui properties
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *brandLabel;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descriptionLabelHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *nameLabelHeightConstraint;

#pragma mark - properties
@property (nonatomic, strong)NSString *image;
@property (nonatomic, strong)NSString *name;
@property (nonatomic, strong)NSString *brand;
@property (nonatomic, strong)NSString *price;
@property (nonatomic, strong)NSString *productDescription;
@end
@implementation BestBuyProductDetailCell

#pragma mark - constructors
-(instancetype)initWithProduct:(id<BestBuyProductProtocol>)product {
    self = [[NSBundle mainBundle] loadNibNamed:NSStringFromClass([self class]) owner:self options:nil].firstObject;
    if (self) {
        [self setName:[product name]];
        [self setBrand:[product brand]];
        [self setProductDescription:[product productDescription]];
        [self setPrice:[NSString stringWithFormat:@"U$%.2f",[[product salePrice] floatValue]]];
        [self setImage:[product image]];
        [self setProductDetailsInfo:NO];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        [[NSNotificationCenter defaultCenter] addObserver:self
    selector:@selector(orientationChanged:)
    name:UIDeviceOrientationDidChangeNotification
    object:nil];
    }
    return self;
}

#pragma mark - setters
-(void)setProductDetailsInfo :(BOOL)isLandscape {
    [self.nameLabel setText:self.name];
    [self.brandLabel setText:self.brand];
    [self.priceLabel setText:self.price];
    [self.descriptionLabel setText:self.productDescription];
    isLandscape ? [self.descriptionLabelHeightConstraint setConstant:[self getLabelHeight:self.descriptionLabel]] : [self.descriptionLabelHeightConstraint setConstant:[self getLabelHeight:self.descriptionLabel]];
    [self.nameLabelHeightConstraint setConstant:[self getLabelHeight:self.nameLabel]];
    [self layoutIfNeeded];
    
}

#pragma mark - layout
- (CGFloat)getLabelHeight:(UILabel*)label
{
    CGSize constraint = CGSizeMake(label.frame.size.width, CGFLOAT_MAX);
    CGSize size;
    
    NSStringDrawingContext *context = [[NSStringDrawingContext alloc] init];
    CGSize boundingBox = [label.text boundingRectWithSize:constraint
                                                  options:NSStringDrawingUsesLineFragmentOrigin
                                               attributes:@{NSFontAttributeName:label.font}
                                                  context:context].size;
    
    size = CGSizeMake(ceil(boundingBox.width), ceil(boundingBox.height));
    
    return size.height;
}

- (void)orientationChanged:(NSNotification *)notification
{
    UIInterfaceOrientation newOrientation =  [UIApplication sharedApplication].statusBarOrientation;
    if ((newOrientation == UIInterfaceOrientationLandscapeLeft || newOrientation == UIInterfaceOrientationLandscapeRight))
    {
        [self setProductDetailsInfo:YES];
    } else {
        [self setProductDetailsInfo:NO];
    }

}


-(CGFloat)tableView:(UIStaticTableView *)tableView heightForCellInIndexPath:(NSIndexPath *)indexPath {
    
    float height = self.nameLabelHeightConstraint.constant + self.descriptionLabelHeightConstraint.constant + self.brandLabel.frame.size.height + self.priceLabel.frame.size.height + 20;
    return height;
}
@end
